# Albatross
Golf Stat Analyzer

---

Copyright 2023, Akshay R. Kapadia<br>
License: GPL-3.0<br>
Version: 0.0.1<br>
Status: Development<br>

---

## Features

- Color coded stats based on PGA Tour average
- Track round by round stats
    - Score
    - Fairways Hit
    - Greens in Regulation
    - Putts Per Round
    - Putts Per GIR
    - Scrambling
    - Sand Saves
    - Penalty Shots
- Track overall stats
    - Rounds Played
    - Scoring Average
    - Driving Accuracy
    - Greens in Regulation
    - Putts Per Round
    - Putts Per GIR
    - Scrambling
    - Sand Saves
    - Penalty Shots Per Round
- Compare your stats to the PGA Tour average or some of the best players in the world
    - Tiger Woods 2000
    - Vijay Singh 2004
    - Tiger Woods 2006
    - Rory McIlroy 2014
    - Jordan Spieth 2015
    - Brooks Koepka 2018

---

## Stat Color Meanings

- Green: above PGA Tour average or better than player in head-to-head
- Yellow: Close to PGA Tour average or player in head-to-head
- Red: Below PGA Tour average or worse than player in head-to-head

---

## Command Screenshots

**Round Scorecard**

![Screenshot 1](./images/screenshot1.png)

**Round History**

![Screenshot 2](./images/screenshot2.png)

**Overall Stats**

![Screenshot 3](./images/screenshot3.png)

**Course Stats**

![Screenshot 4](./images/screenshot4.png)

**Stat Comparison Against Best Seasons On PGA Tour**

![Screenshot 5](./images/screenshot5.png)