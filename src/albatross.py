import sys
import tableprint
from colorama import Fore, Style
from pick import pick
from ModelProfile import ModelProfile
from Hole import Hole
from Functions import validateType, validateValue
from Constants import *
from PlayerProfile import PlayerProfile
from Round import Round
from datetime import datetime
import calendar


def dateSelector() -> str:
    """
    Returns a date string for the user selected date
    """

    today = datetime.today()

    # Select year from the past 4 years
    year, _yearIndex = pick(range(int(today.strftime(
        "%Y")) - 4, int(today.strftime("%Y")) + 1), "Year", indicator="=>", default_index=4)
    validateType(year, int)

    # Only show current and past months for this year
    if str(year) == today.strftime("%Y"):
        _month, monthIndex = pick(MONTHS[:int(today.strftime(
            "%m"))], "Month", indicator="=>", default_index=int(today.strftime("%m")) - 1)
        validateType(monthIndex, int)
        # Only show current and past days for current month
        if monthIndex == (int(today.strftime("%m")) - 1):
            day, _dayIndex = pick(range(1, int(today.strftime(
                "%d")) + 1), "Day", indicator="=>", default_index=int(today.strftime("%d")) - 1)
            validateType(day, int)
        else:
            day, _dayIndex = pick(range(1, calendar.monthrange(
                int(year), monthIndex + 1)[1] + 1), "Day", indicator="=>", default_index=0)
            validateType(day, int)

        return str(monthIndex + 1) + "/" + str(day) + "/" + str(year)
    else:
        _month, monthIndex = pick(
            MONTHS, "Month", indicator="=>", default_index=0)
        validateType(monthIndex, int)

        day, _dayIndex = pick(range(1, calendar.monthrange(
            int(year), monthIndex + 1)[1] + 1), "Day", indicator="=>", default_index=0)
        validateType(day, int)

        return str(monthIndex + 1) + "/" + str(day) + "/" + str(year)


def courseTemplateSelector(player: PlayerProfile, newRound: bool = False) -> int:
    """
    Returns the user selected round index
    """

    if len(player.courseTemplates) > 0:
        # Show all courseIDs with the date of the most recent round
        options = [
            courseTemplate.courseID + " (Last Played: " + player.getRoundsByRecency(1, player.getRoundsByCourseID(courseTemplate.courseID))[0].date + ")" for courseTemplate in player.courseTemplates]
        # Option to add new course for new rounds
        if newRound:
            options += ["New Course"]

        _courseTemplate, courseTemplateIndex = pick(
            options, "Select Course Template", indicator="=>", default_index=0)
        validateType(courseTemplateIndex, int)

        return courseTemplateIndex
    else:
        # Return length of courseTemplates list if no rounds have been entered
        return len(player.courseTemplates)


def addRound(player: PlayerProfile) -> None:
    """
    Creates a new round from user input and adds it to the PlayerProfile
    """

    date = dateSelector()
    courseTemplateIndex = courseTemplateSelector(player, newRound=True)

    # Check if user wants to add new course or if this is the first course
    if courseTemplateIndex == len(player.courseTemplates):
        while True:
            try:
                courseID = input("Course ID (5 Characters Max): ")
                if (len(courseID) > 5) or (courseID in [new_round.courseID for new_round in player.rounds]):
                    raise ValueError
                else:
                    break
            except ValueError:
                print(f"\n{Fore.RED}Invalid Course ID{Style.RESET_ALL}\n")
    else:
        # Use existing courseTemplate
        courseTemplate = player.courseTemplates[courseTemplateIndex]
        courseID = courseTemplate.courseID

    holes = []

    # Get user input for each hole
    for i in range(1, 19):
        if courseTemplateIndex == len(player.courseTemplates):
            par, _index = pick([3, 4, 5], getBanner(
                "Hole: " + str(i) + "\nPar:"), indicator="=>", default_index=1)
            validateType(par, int)
        else:
            par = courseTemplate.pars[i-1]

        score, _index = pick(range(1, 15), getBanner(
            "Hole: " + str(i) + "\nScore:"), indicator="=>", default_index=(par-1))
        validateType(score, int)
        validateValue(score, 1, 15)

        # Value of -1 means N/A, 0 means fail, and 1 means success

        if par > 3:
            fairway, _index = pick([True, False], getBanner(
                "Hole: " + str(i) + "\nFairway Hit:"), indicator="=>", default_index=0)
            validateType(fairway, bool)
            fairway = int(fairway)
        else:
            fairway = -1

        green_in_regulation, _index = pick([True, False], getBanner(
            "Hole: " + str(i) + "\nGreen In Regulation:"), indicator="=>", default_index=0)
        validateType(green_in_regulation, bool)
        green_in_regulation = int(green_in_regulation)

        if not green_in_regulation:
            if score <= par:
                scramble = 1
            else:
                scramble = 0
        else:
            scramble = -1

        if score < par and not green_in_regulation:
            putts = 0
        else:
            putts, _index = pick(range(score), getBanner(
                "Hole: " + str(i) + "\nPutts:"), indicator="=>", default_index=(1 if score > 1 else 0))
            validateType(putts, int)
            validateValue(putts, 0, score - 1)

        if not green_in_regulation:
            bunker_shot, _index = pick([True, False], getBanner(
                "Hole: " + str(i) + "\nGreenside Bunker Shot:"), indicator="=>", default_index=0)
            validateType(bunker_shot, bool)
            if bunker_shot:
                sand_save = 1 if scramble else 0
        else:
            sand_save = -1

        penalty_shots, _index = pick(range(score/2), getBanner(
            "Hole: " + str(i) + "\nPenalty Shots:"), indicator="=>", default_index=0)
        validateType(penalty_shots, int)
        validateValue(penalty_shots, 0, (score/2) - 1)

        # Create new Hole object and append to the holes list
        holes.append(Hole(i, par, score, fairway, green_in_regulation,
                     putts, scramble, sand_save, penalty_shots))

    # Create new Round object
    new_round = Round(date, courseID, len(player.rounds) + 1, holes)

    # Update player object with new round
    player.update(new_round)

    # Save data to CSV file
    player.write_to_csv()

    # Print the hole-by-hole stats
    holeByHoleStats = new_round.getHoleByHoleStatsForTable()

    print("\nScorecard For Round #" + str(new_round.round_number) +
          " At " + new_round.courseID + " On " + new_round.date)

    tableprint.table(holeByHoleStats, HOLE_HEADER, width=14, style="round")

    # Print the overall player stats
    stats(player)


def stats(player: PlayerProfile) -> None:
    """
    Prints overall user stats
    """

    # If there are more than 20 rounds then use the 20 most recent rounds for stats
    if len(player.rounds) > 20:
        recentRounds = player.getRoundsByRecency(20)
        playerStats, extraPlayerStats = player.getStatsForTable(
            filteredRounds=recentRounds)

        print("\nStats For 20 Most Recent Rounds")
        tableprint.table([playerStats, extraPlayerStats],
                         PLAYER_HEADER, width=16, style="round")

    # Print overall stats for all rounds
    playerStats, extraPlayerStats = player.getStatsForTable()

    print("\nOverall Stats")
    tableprint.table([playerStats, extraPlayerStats],
                     PLAYER_HEADER, width=16, style="round")


def courseStats(player: PlayerProfile) -> None:
    """
    Prints user course stats
    """

    courseTemplateIndex = courseTemplateSelector(player)
    courseID = player.courseTemplates[courseTemplateIndex].courseID

    front9, back9 = player.getCourseStatsForTable(courseID)

    print("\nHole By Hole Stats For " + courseID)

    # Print front 9 stats
    tableprint.table(front9, FRONT9_HEADER, width=11, style="round")

    # Print back 9 stats
    tableprint.table(back9, BACK9_HEADER, width=11, style="round")

    # Print overall stats for the course
    filteredRounds = player.getRoundsByCourseID(courseID)
    playerStats, extraPlayerStats = player.getStatsForTable(filteredRounds)

    print("\nOverall Stats At " + courseID)

    tableprint.table([playerStats, extraPlayerStats],
                     PLAYER_HEADER, width=16, style="round")


def rounds(player: PlayerProfile) -> None:
    """
    Prints each individual round's stats
    """

    # Get stats for each individual round
    data = [new_round.getStatsForTable(
        includeCourseID=False) for new_round in player.rounds]

    print("\nRound History (" + str(len(player.rounds)) + " Rounds)")

    tableprint.table(data, ROUND_HEADER, width=14, style="round")


def scorecards(player: PlayerProfile) -> None:
    """
    Prints all scorecard data for the selected round
    """

    options = [(str(i + 1) + ". " + player.rounds[i].date + " @ " + player.rounds[i].courseID + " (" +
                str(player.rounds[i].score) + ")") for i in range(len(player.rounds))]

    _roundNumber, roundNumberIndex = pick(
        options, "Select A Round", indicator="=>", default_index=0)
    validateType(roundNumberIndex, int)
    validateValue(roundNumberIndex, 0, len(player.rounds) - 1)

    # Get round from user selected round number
    new_round = player.rounds[roundNumberIndex]

    holeByHoleStats = player.getHoleByHoleStatsForTable(roundNumberIndex)

    print("\nScorecard For Round #" + str(roundNumberIndex + 1) +
          " At " + new_round.courseID + " On " + new_round.date)

    tableprint.table(holeByHoleStats, HOLE_HEADER, width=14, style="round")


def head_to_head(player: PlayerProfile) -> None:
    """
    Compares user data to selected model
    """

    # Available player models to select from
    models = [
        ModelProfile(
            "Tiger Woods 2000", 71.22, 75.15, 67.08, 57.27, 28.76, 67.79),
        ModelProfile(
            "Vijay Singh 2004", 60.36, 73.03, 62.36, 50.86, 29.24, 68.84),
        ModelProfile(
            "Tiger Woods 2006", 60.71, 74.15, 62.81, 55.17, 29.38, 68.12),
        ModelProfile(
            "Rory McIlroy 2014", 59.93, 69.44, 58.52, 47.50, 28.59, 68.83),
        ModelProfile(
            "Jordan Spieth 2015", 62.91, 67.87, 65.03, 58.14, 27.82, 68.93),
        ModelProfile(
            "Brooks Koepka 2018", 56.85, 68.28, 63.28, 53.76, 28.76, 69.44)
    ]

    options = [
        "Tiger Woods 2000",
        "Vijay Singh 2004",
        "Tiger Woods 2006",
        "Rory McIlroy 2014",
        "Jordan Spieth 2015",
        "Brooks Koepka 2018"
    ]

    modelOption, modelIndex = pick(
        options, "Select One", indicator="=>", default_index=0)
    validateType(modelOption, str)
    validateType(modelIndex, int)

    # Convert PlayerProfile into ModelProfile
    you = player.getModelProfile()

    # Compare stats between player and selected model
    comp, adv_comp = you.getStatsCompForTable(models[modelIndex])

    comp = ["Diff"] + comp

    print("\nHead-To-Head Stat Comparison VS " + modelOption)

    tableprint.table([models[modelIndex].getStatsForTable(), you.getStatsForTable(
    ), comp, adv_comp], MODEL_HEADER, width=18, style="round")


def deleteRound(player: PlayerProfile) -> None:
    """
    Deletes the selected round
    """

    options = [(str(i + 1) + ". " + player.rounds[i].date + " @ " + player.rounds[i].courseID + " (" +
                str(player.rounds[i].score) + ")") for i in range(len(player.rounds))] + ["Quit"]

    roundStr, roundNumberIndex = pick(
        options, "Select A Round To Delete", indicator="=>", default_index=len(options)-1)
    validateType(roundStr, str)
    validateType(roundNumberIndex, int)

    # Checks if the user didn't select 'Quit'
    if roundStr != "Quit":
        round = player.rounds[roundNumberIndex]

        # Confirmation prompt
        confirm, confirmIndex = pick(
            ["Yes", "No"], "Are You Sure You Want To Delete Your Round On " + round.date + " @ " + round.courseID, indicator="=>", default_index=1)
        validateType(confirm, str)

        if confirm == "Yes":
            player.deleteRound(roundNumberIndex)

            # Save data to CSV file
            player.write_to_csv()

            print(f"\n\n{Fore.RED}Deleted Round #" +
                  str(roundNumberIndex + 1) + f"{Style.RESET_ALL}\n")

            # Print updated round history
            rounds(player)

        else:
            print("\nRound Deletion Canceled")
    else:
        print("\nRound Deletion Canceled")

# TODO for v0.0.2
# def editRound(player: PlayerProfile) -> None:
#     roundIndex = roundSelector(player)
#     round = player.rounds[roundIndex]

# def select(options, title, default_index=0):
#     while True:
#         try:
#             for i in range(len(options)):
#                     print(str(i+1) + ") " + str(options[i]))
#             selection = input("\nSelect (Default " + str(default_index + 1) + "): ")
#             if selection == "":
#                 return options[default_index]
#             elif int(selection) in range(len(options)+1):
#                 return options[int(selection) - 1]
#             else:
#                 raise ValueError
#         except:
#             print("Error")


def getFormattedBanner() -> None:
    """
    Prints formatted Albatross banner
    """

    print(
        f"\n{Fore.BLUE}______________________________________________________________________________________________\
{Style.RESET_ALL}")
    print(f"""{Fore.BLUE}
\t █████╗ ██╗     ██████╗  █████╗ ████████╗██████╗  ██████╗ ███████╗███████╗
\t██╔══██╗██║     ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔═══██╗██╔════╝██╔════╝
\t███████║██║     ██████╔╝███████║   ██║   ██████╔╝██║   ██║███████╗███████╗
\t██╔══██║██║     ██╔══██╗██╔══██║   ██║   ██╔══██╗██║   ██║╚════██║╚════██║
\t██║  ██║███████╗██████╔╝██║  ██║   ██║   ██║  ██║╚██████╔╝███████║███████║
\t╚═╝  ╚═╝╚══════╝╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝{Style.RESET_ALL}""")
    print(
        f"\n{Fore.GREEN}\t\t\t   -</Golf Statistical Analyzer/>-{Style.RESET_ALL}")
    print(
        f"\n{Fore.BLUE}______________________________________________________________________________________________\
{Style.RESET_ALL}")


def getBanner(subtext: str = "") -> None:
    """
    Prints Albatross banner with subtext
    """

    banner = """
______________________________________________________________________________________________

 █████╗ ██╗     ██████╗  █████╗ ████████╗██████╗  ██████╗ ███████╗███████╗
██╔══██╗██║     ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔═══██╗██╔════╝██╔════╝
███████║██║     ██████╔╝███████║   ██║   ██████╔╝██║   ██║███████╗███████╗
██╔══██║██║     ██╔══██╗██╔══██║   ██║   ██╔══██╗██║   ██║╚════██║╚════██║
██║  ██║███████╗██████╔╝██║  ██║   ██║   ██║  ██║╚██████╔╝███████║███████║
╚═╝  ╚═╝╚══════╝╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝
______________________________________________________________________________________________

"""

    return banner + subtext


def main() -> None:
    """
    Runs the program
    """
    try:
        getFormattedBanner()

        player = PlayerProfile()

        while True:

            options = [
                "Add New Round",
                "Quit"
            ]

            initial = True

            if len(player.rounds) > 0:
                initial = False
                options = [
                    "Add New Round",
                    "Delete Round",
                    # "Edit Round",
                    "Round History",
                    "Overall Stats",
                    "Course Stats",
                    "Scorecards",
                    "Head-To-Head PGA Tour",
                    "Quit"
                ]

            _option, optionIndex = pick(options, getBanner(),
                                        indicator="=>", default_index=0)
            validateType(optionIndex, int)

            if optionIndex == 0:
                addRound(player)
            elif optionIndex == 1:
                deleteRound(player)
            # elif optionIndex == 2:
            #     editRound(player)
            elif optionIndex == 2:
                rounds(player)
            elif optionIndex == 3:
                stats(player)
            elif optionIndex == 4:
                courseStats(player)
            elif optionIndex == 5:
                scorecards(player)
            elif optionIndex == 6:
                head_to_head(player)
            else:
                print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}\n")
                sys.exit()

            input(f"\n{Fore.RED}Hit Enter To Continue{Style.RESET_ALL}\n")
    except ValueError as e:
        print(e)
        print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}\n")
    except TypeError as e:
        print(e)
        print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}\n")
    except Exception as e:
        print(e)
        print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}\n")


if __name__ == "__main__":
    main()
