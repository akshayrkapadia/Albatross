from __future__ import annotations
from colorama import Fore, Style
from Functions import formatString
from Constants import *


class ModelProfile:
    """
    Class to hold data for each PGA Tour Player/Season
    """

    def __init__(
            self,
            name: str,
            fairways_hit_percentage: float,
            greens_in_regulation_percentage: float,
            scrambling_percentage: float,
            sand_saves_percentage: float,
            putts_per_round: float,
            scoring_average: float) -> None:

        self.name = name
        self.fairways_hit_percentage = fairways_hit_percentage
        self.greens_in_regulation_percentage = greens_in_regulation_percentage
        self.scrambling_percentage = scrambling_percentage
        self.sand_saves_percentage = sand_saves_percentage
        self.putts_per_round = putts_per_round
        self.scoring_average = scoring_average

    def getStatsForTable(self) -> None:
        """
        Returns all the stats for the model
        """

        data = [
            self.name,
            formatString(
                self.scoring_average, PGA_SCORING_AVG, PGA_SCORING_CLOSE_RANGE, reverse=True),
            formatString(self.fairways_hit_percentage, PGA_FAIRWAYS_AVG,
                         PGA_FAIRWAYS_CLOSE_RANGE, outputString=str(self.fairways_hit_percentage) + "%"),
            formatString(self.greens_in_regulation_percentage, PGA_GIR_AVG,
                         PGA_GIR_CLOSE_RANGE, outputString=str(self.greens_in_regulation_percentage) + "%"),
            formatString(self.scrambling_percentage, PGA_SCRAMBLING_AVG,
                         PGA_SCRAMBLING_CLOSE_RANGE, outputString=str(self.scrambling_percentage) + "%"),
            formatString(self.sand_saves_percentage, PGA_SAND_SAVE_AVG,
                         PGA_SAND_SAVE_CLOSE_RANGE, outputString=str(self.sand_saves_percentage) + "%"),
            formatString(
                self.putts_per_round, PGA_PUTTS_AVG, PGA_PUTTS_CLOSE_RANGE, reverse=True)
        ]

        return data

    def compare(self, model_profile: ModelProfile) -> list[float]:
        """
        Compares user model data to the current model data
        """

        fairways_hit_percent_comp = round(
            self.fairways_hit_percentage -
            model_profile.fairways_hit_percentage,
            2)
        fairways_hit_comp = round((fairways_hit_percent_comp / 100) * 14, 2)

        greens_in_regulation_percent_comp = round(
            self.greens_in_regulation_percentage -
            model_profile.greens_in_regulation_percentage,
            2)
        greens_in_regulation_comp = round(
            (greens_in_regulation_percent_comp / 100) * 18, 2)

        scrambling_percent_comp = -1 if self.scrambling_percentage == -1 else round(
            self.scrambling_percentage - model_profile.scrambling_percentage, 2)
        scrambling_comp = -1 if scrambling_percent_comp == -1 else round(
            (scrambling_percent_comp / 100) * (((100 - self.greens_in_regulation_percentage) / 100) * 18), 2)

        sand_saves_percent_comp = -1 if self.sand_saves_percentage == -1 else round(
            self.sand_saves_percentage - model_profile.sand_saves_percentage, 2)
        sand_saves_comp = -1 if sand_saves_percent_comp == -1 else round(
            (sand_saves_percent_comp / 100) * (((100 - self.greens_in_regulation_percentage) / 100) * 18), 2)

        putts_comp = round(
            self.putts_per_round -
            model_profile.putts_per_round,
            2)

        score_comp = round(
            self.scoring_average -
            model_profile.scoring_average,
            2)

        return [fairways_hit_percent_comp, fairways_hit_comp, greens_in_regulation_percent_comp,
                greens_in_regulation_comp, scrambling_percent_comp, scrambling_comp, sand_saves_percent_comp, sand_saves_comp, putts_comp, score_comp]

    def getStatsCompForTable(self, model_profile: ModelProfile) -> (list[str], list[str]):
        """
        Returns all the comparison stats
        """

        fairways_hit_percent_comp, fairways_hit_comp, greens_in_regulation_percent_comp, greens_in_regulation_comp, \
            scrambling_percent_comp, scrambling_comp, sand_saves_percent_comp, sand_saves_comp, putts_comp, score_comp = self.compare(
                model_profile)

        data = [
            self.getFormattedCompString(score_comp, PGA_SCORING_CLOSE_RANGE,
                                        outputString=str(abs(score_comp)) + " Strokes", reverse=True),
            self.getFormattedCompString(fairways_hit_percent_comp, PGA_FAIRWAYS_CLOSE_RANGE,
                                        outputString=str(abs(fairways_hit_comp)) + " Fairways/Rd"),
            self.getFormattedCompString(greens_in_regulation_percent_comp, PGA_GIR_CLOSE_RANGE,
                                        outputString=str(abs(greens_in_regulation_comp)) + " Greens/Rd"),
            self.getFormattedCompString(scrambling_percent_comp, PGA_SCRAMBLING_CLOSE_RANGE,
                                        outputString=str(abs(scrambling_comp)) + " Scrambles/Rd"),
            self.getFormattedCompString(sand_saves_percent_comp, PGA_SAND_SAVE_CLOSE_RANGE,
                                        outputString=str(abs(sand_saves_comp)) + " Saves/Rd"),
            self.getFormattedCompString(putts_comp, PGA_PUTTS_CLOSE_RANGE,
                                        outputString=str(abs(putts_comp)) + " Putts", reverse=True)
        ]

        extraData = [
            "",
            "",
            self.getFormattedCompString(fairways_hit_percent_comp, PGA_FAIRWAYS_CLOSE_RANGE,
                                        outputString=str(abs(fairways_hit_percent_comp)) + "%"),
            self.getFormattedCompString(greens_in_regulation_percent_comp, PGA_GIR_CLOSE_RANGE,
                                        outputString=str(abs(greens_in_regulation_percent_comp)) + "%"),
            self.getFormattedCompString(scrambling_percent_comp, PGA_SCRAMBLING_CLOSE_RANGE,
                                        outputString=str(abs(scrambling_percent_comp)) + "%"),
            self.getFormattedCompString(sand_saves_percent_comp, PGA_SAND_SAVE_CLOSE_RANGE,
                                        outputString=str(abs(sand_saves_percent_comp)) + "%"),
            ""
        ]

        return data, extraData

    def getFormattedCompString(self, value: float, closeRange: float, outputString: str = "", reverse: bool = False) -> str:
        """ 
        Returns a color coded formatted string for the given comparison stat
        """

        if outputString == "":
            outputString = str(value)
        if reverse:
            if value == -1:
                return "-"
            elif value <= 0:
                return f"{Fore.GREEN}-" + str(outputString) + f"{Style.RESET_ALL}"
            elif value > closeRange:
                return f"{Fore.RED}+" + str(outputString) + f"{Style.RESET_ALL}"
            else:
                return f"{Fore.YELLOW}+" + str(outputString) + f"{Style.RESET_ALL}"
        else:
            if value == -1:
                return "-"
            if value >= 0:
                return f"{Fore.GREEN}+" + str(outputString) + f"{Style.RESET_ALL}"
            elif value < closeRange:
                return f"{Fore.RED}-" + str(outputString) + f"{Style.RESET_ALL}"
            else:
                return f"{Fore.YELLOW}-" + str(outputString) + f"{Style.RESET_ALL}"
