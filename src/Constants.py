
PGA_FAIRWAYS_AVG = 58.50
PGA_FAIRWAYS_CLOSE_RANGE = 5
PGA_GIR_AVG = 65.11
PGA_GIR_CLOSE_RANGE = 5
PGA_SCRAMBLING_AVG = 58.42
PGA_SCRAMBLING_CLOSE_RANGE = 5
PGA_PUTTS_AVG = 28.98
PGA_PUTTS_CLOSE_RANGE = 2
PGA_PUTTS_PER_GIR = 1.69
PGA_PUTTS_PER_GIR_CLOSE_RANGE = 0.1
PGA_SCORING_AVG = 71.16
PGA_SCORING_CLOSE_RANGE = 3
PGA_SAND_SAVE_AVG = 49.11
PGA_SAND_SAVE_CLOSE_RANGE = 5
PGA_PENALTY_SHOTS_AVG = 0.5
PGA_PENALTY_SHOTS_CLOSE_RANGE = 1

MONTHS = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]

HOLE_HEADER = [
    "Hole #",
    "Par",
    "Score",
    "Fairway Hit",
    "GIR",
    "Putts",
    "Scramble",
    "Sand Save",
    "Penalty Shots"
]

ROUND_HEADER = [
    "Date",
    # "ID",
    "Par",
    "Score",
    "Fairways Hit",
    "GIR",
    "Putts",
    "Scrambling",
    "Sand Saves",
    "Penalty Shots"
]

MODEL_HEADER = [
    "Name",
    "Scoring Average",
    "Driving Accuracy",
    "GIR",
    "Scrambling",
    "Sand Saves",
    "Putting Average"
]

PLAYER_HEADER = [
    "Total Rounds",
    "Scoring Average",
    "Driving Accuracy",
    "GIR",
    "Scrambling",
    "Sand Saves",
    "Putting Average",
    "Penalty Shots"
]


COMPARE_HEADER = [
    "Fairways",
    "Greens",
    "Up & Downs",
    "Putts",
    "Strokes"
]

FRONT9_HEADER = ["", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Total"]

BACK9_HEADER = ["", "10", "11", "12", "13",
                "14", "15", "16", "17", "18", "Total"]
