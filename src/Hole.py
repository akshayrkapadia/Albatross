from colorama import Fore, Style


class Hole:
    """
    Class for holding data for an individual hole
    """

    def __init__(self, hole_number: int, par: int, score: int, fairway: int, green_in_regulation: int, putts: int, scramble: int, sand_save: int, penalty_shots: int) -> None:

        self.hole_number = hole_number
        self.par = par
        self.score = score

        # -1 means N/A, 0 means failure, 1 means success
        self.fairway = fairway
        self.green_in_regulation = green_in_regulation
        self.putts = putts
        self.scramble = scramble
        self.sand_save = sand_save

        self.penalty_shots = penalty_shots

    def getStatsForTable(self) -> list:
        """
        Converts each value into a color coded string
        """

        score = str(self.score) if self.score == self.par else f"{Fore.GREEN}(" + str(self.score) + f"){Style.RESET_ALL}" if (self.score - self.par) == -1 else f"{Fore.GREEN}((" + str(self.score) + f")){Style.RESET_ALL}" if (
            self.score - self.par) <= -2 else f"{Fore.RED}[" + str(self.score) + f"]{Style.RESET_ALL}" if (self.score - self.par) == 1 else f"{Fore.RED}[[" + str(self.score) + f"]]{Style.RESET_ALL}"
        fairway = "-" if self.fairway == - \
            1 else f"{Fore.GREEN}${Style.RESET_ALL}" if self.fairway else f"{Fore.RED}X{Style.RESET_ALL}"
        green_in_regulation = f"{Fore.GREEN}${Style.RESET_ALL}" if self.green_in_regulation else f"{Fore.RED}X{Style.RESET_ALL}"
        scramble = "-" if self.scramble == - \
            1 else f"{Fore.GREEN}${Style.RESET_ALL}" if self.scramble else f"{Fore.RED}X{Style.RESET_ALL}"
        sand_save = "-" if self.sand_save == - \
            1 else f"{Fore.GREEN}${Style.RESET_ALL}" if self.sand_save else f"{Fore.RED}X{Style.RESET_ALL}"
        putts = str(self.putts) if self.putts == 2 else f"{Fore.GREEN}" + str(
            self.putts) + f"{Style.RESET_ALL}" if self.putts < 2 else f"{Fore.RED}" + str(self.putts) + f"{Style.RESET_ALL}"
        penalty_shots = f"{Fore.GREEN}" + str(self.penalty_shots) + f"{Style.RESET_ALL}" if self.penalty_shots == 0 else f"{Fore.RED}" + str(
            self.penalty_shots) + f"{Style.RESET_ALL}"

        data = [
            self.hole_number,
            self.par,
            score,
            fairway,
            green_in_regulation,
            putts,
            scramble,
            sand_save,
            penalty_shots
        ]

        return data

    def getStatsForCSV(self) -> list:
        """
        Returns all the stats that will be saved to a CSV file
        """

        data = [
            self.hole_number,
            self.par,
            self.score,
            self.fairway,
            self.green_in_regulation,
            self.putts,
            self.scramble,
            self.sand_save,
            self.penalty_shots
        ]

        return data
