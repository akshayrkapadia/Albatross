import csv
import sys
import tableprint
from os.path import isfile
from colorama import Fore, Style

PGA_FAIRWAYS_AVG = .58
PGA_FAIRWAYS_CLOSE_RANGE = 0.05
PGA_GIR_AVG = .65
PGA_GIR_CLOSE_RANGE = 0.05
PGA_SCRAMBLING_AVG = .57
PGA_SCRAMBLING_CLOSE_RANGE = 0.05
PGA_PUTTS_AVG = 29
PGA_PUTTS_CLOSE_RANGE = 3
PGA_SCORING_AVG = 71
PGA_SCORING_CLOSE_RANGE = 3

ROUND_HEADER = [
    "Round #", 
    "Score", 
    "Fairways Hit", 
    "GIR", 
    "Scrambling",
    "Putts"
    ]

MODEL_HEADER = [
    "Name",
    "Scoring Average",
    "Driving Accuracy",
    "GIR",
    "Scrambling",
    "Putting Average"
]

PLAYER_HEADER = [
    "Total Rounds",
    "Scoring Average",
    "Driving Accuracy",
    "GIR",
    "Scrambling",
    "Putting Average"
]


COMPARE_HEADER = [
    "Fairways",
    "Greens",
    "Up & Downs",
    "Putts",
    "Strokes"
]


class Round:

    """
    Class for holding data for each round
    """

    def __init__(
            self,
            fairways_hit,
            fairways_possible,
            greens_in_regulation,
            up_and_downs,
            putts,
            score,
            round_number):

        self.fairways_hit = fairways_hit
        self.fairways_possible = fairways_possible
        self.fairways_hit_percentage = round(
            (fairways_hit / fairways_possible) * 100, 2)
        self.greens_in_regulation = greens_in_regulation
        self.greens_in_regulation_percentage = round(
            (greens_in_regulation / 18) * 100, 2)
        self.up_and_downs = up_and_downs
        self.scrambling_percentage = None if (greens_in_regulation == 18) else round(
            ((up_and_downs / (18 - greens_in_regulation)) * 100), 2)
        self.putts = putts
        self.score = score
        self.round_number = round_number

    def print_stats_str(self):
        """
        Prints all the stats for the round
        """

        data = []

        data.append(str(self.round_number))

        if self.score <= PGA_SCORING_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.score) + f"{Style.RESET_ALL}")
        elif self.score > (PGA_SCORING_AVG + PGA_SCORING_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.score) + f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.score) + f"{Style.RESET_ALL}")

        if round(
                self.fairways_hit /
                self.fairways_possible,
                2) >= PGA_FAIRWAYS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.GREEN}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")
        elif round(self.fairways_hit / self.fairways_possible, 2) < (PGA_FAIRWAYS_AVG - PGA_FAIRWAYS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.RED}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.YELLOW}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")

        if round(self.greens_in_regulation / 18, 2) >= PGA_GIR_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.greens_in_regulation) +
                  f"/18 {Style.RESET_ALL}({Fore.GREEN}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        elif round(self.greens_in_regulation / 18, 2) < (PGA_GIR_AVG - PGA_GIR_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.greens_in_regulation) +
                  f"/18 {Style.RESET_ALL}({Fore.RED}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.greens_in_regulation) +
                  f"/18 {Style.RESET_ALL}({Fore.YELLOW}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        if self.scrambling_percentage is None:
            data.append(f"{Fore.RED}No data{Style.RESET_ALL}")
        else:
            if round(self.up_and_downs / (18 - self.greens_in_regulation),
                     2) >= PGA_SCRAMBLING_AVG:
                data.append(f"{Fore.GREEN}" +
                      str(self.up_and_downs) +
                      "/" +
                      str(18 -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.GREEN}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")
            elif round(self.up_and_downs / (18 - self.greens_in_regulation), 2) < (
                    PGA_SCRAMBLING_AVG - PGA_SCRAMBLING_CLOSE_RANGE):
                data.append(f"{Fore.RED}" +
                      str(self.up_and_downs) +
                      "/" +
                      str(18 -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.RED}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")
            else:
                data.append(f"{Fore.YELLOW}" +
                      str(self.up_and_downs) +
                      "/" +
                      str(18 -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.YELLOW}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")

        if self.putts <= PGA_PUTTS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.putts) + f"{Style.RESET_ALL}")
        elif self.putts > (PGA_PUTTS_AVG + PGA_PUTTS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.putts) + f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.putts) + f"{Style.RESET_ALL}")
    
        return data

class ModelProfile:

    """
    Class to hold data for each PGA Tour Player/Season
    """

    def __init__(
            self,
            name,
            fairways_hit_percentage,
            greens_in_regulation_percentage,
            scrambling_percentage,
            putts_per_round,
            scoring_average):

        self.name = name
        self.fairways_hit_percentage = fairways_hit_percentage
        self.greens_in_regulation_percentage = greens_in_regulation_percentage
        self.scrambling_percentage = scrambling_percentage
        self.putts_per_round = putts_per_round
        self.scoring_average = scoring_average

    def print_stats_str(self):
        """
        Prints all the stats for model
        """

        data = []

        data.append(self.name)

        if self.scoring_average < PGA_SCORING_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")
        elif self.scoring_average > (PGA_SCORING_AVG + PGA_SCORING_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")

        if self.fairways_hit_percentage >= PGA_FAIRWAYS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL}")
        elif self.fairways_hit_percentage < (PGA_FAIRWAYS_AVG - PGA_FAIRWAYS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL}")

        if self.greens_in_regulation_percentage >= PGA_GIR_AVG:
            data.append(f"{Fore.GREEN}" + str(
                self.greens_in_regulation_percentage) + f"%{Style.RESET_ALL}")
        elif self.greens_in_regulation_percentage < (PGA_GIR_AVG - PGA_GIR_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" + str(
                self.greens_in_regulation_percentage) + f"%{Style.RESET_ALL}")

        if self.scrambling_percentage >= PGA_SCRAMBLING_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.scrambling_percentage) +
                  f"%{Style.RESET_ALL}")
        elif self.scrambling_percentage < (PGA_SCRAMBLING_AVG - PGA_SCRAMBLING_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.scrambling_percentage) +
                  f"%{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.scrambling_percentage) +
                  f"%{Style.RESET_ALL}")

        if self.putts_per_round <= PGA_PUTTS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL}")
        elif self.putts_per_round > (PGA_PUTTS_AVG + PGA_PUTTS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL}")
            
        return data


class PlayerProfile:

    """
    Class to hold user data
    """

    def __init__(self):

        self.fairways_hit = 0
        self.fairways_possible = 0
        self.fairways_hit_percentage = None
        self.greens_in_regulation = 0
        self.greens_in_regulation_percentage = 0
        self.up_and_downs = 0
        self.scrambling_percentage = None
        self.putts = 0
        self.putts_per_round = 0
        self.putts_per_hole = 0
        self.scoring_average = 0
        self.total_rounds = 0
        self.rounds = []

        if isfile("albatross_data.csv"):
            with open("albatross_data.csv", "r+", newline="", encoding="utf-8") as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=",")
                line_count = 0
                for row in csv_reader:
                    if line_count > 0:
                        new_round = Round(
                            int(
                                row[0]), int(
                                row[1]), int(
                                row[2]), int(
                                row[3]), int(
                                row[4]), int(
                                row[5]), line_count)
                        self.update(new_round)
                    line_count += 1

    def update(self, new_round):
        """
        Updates class fields with new round
        """

        self.rounds.append(new_round)
        self.total_rounds += 1
        self.fairways_hit += new_round.fairways_hit
        self.fairways_possible += new_round.fairways_possible
        self.fairways_hit_percentage = round(
            (self.fairways_hit / self.fairways_possible) * 100, 2)
        self.greens_in_regulation += new_round.greens_in_regulation
        self.greens_in_regulation_percentage = round(
            (self.greens_in_regulation / (self.total_rounds * 18)) * 100, 2)
        self.up_and_downs += 0 if new_round.up_and_downs is None else new_round.up_and_downs
        self.scrambling_percentage = self.scrambling_percentage if (new_round.greens_in_regulation == 18) else round(
            ((self.up_and_downs / ((self.total_rounds * 18) - self.greens_in_regulation)) * 100), 2)
        self.putts += new_round.putts
        self.putts_per_round = round(
            ((self.putts_per_round * (self.total_rounds - 1)) + new_round.putts) / self.total_rounds, 2)
        self.putts_per_hole = round(self.putts_per_round / 18, 2)
        self.scoring_average = round(
            ((self.scoring_average * (self.total_rounds - 1)) + new_round.score) / self.total_rounds, 2)

    def compare(self, model_profile):
        """
        Compares user data to model data
        """

        fairways_hit_percent_comp = round(
            self.fairways_hit_percentage -
            model_profile.fairways_hit_percentage,
            2)
        fairways_hit_comp = round((fairways_hit_percent_comp / 100) * 14, 2)
        greens_in_regulation_percent_comp = round(
            self.greens_in_regulation_percentage -
            model_profile.greens_in_regulation_percentage,
            2)
        greens_in_regulation_comp = round(
            (greens_in_regulation_percent_comp / 100) * 18, 2)
        scrambling_percent_comp = None if self.scrambling_percentage is None else round(
            self.scrambling_percentage - model_profile.scrambling_percentage, 2)
        scrambling_comp = None if scrambling_percent_comp is None else round(
            (scrambling_percent_comp / 100) * (((100 - self.greens_in_regulation_percentage) / 100) * 18), 2)
        putts_comp = round(
            self.putts_per_round -
            model_profile.putts_per_round,
            2)
        score_comp = round(
            self.scoring_average -
            model_profile.scoring_average,
            2)

        return fairways_hit_percent_comp, fairways_hit_comp, greens_in_regulation_percent_comp, \
            greens_in_regulation_comp, scrambling_percent_comp, scrambling_comp, putts_comp, score_comp

    def print_comp_str(self, model_profile):
        """
        Prints all the comparision stats
        """

        data = []

        fairways_hit_percent_comp, fairways_hit_comp, greens_in_regulation_percent_comp, greens_in_regulation_comp, \
            scrambling_percent_comp, scrambling_comp, putts_comp, score_comp = self.compare(
                model_profile)
        if fairways_hit_percent_comp >= 0:
            data.append(f"{Fore.GREEN}+" +
                  str(abs(fairways_hit_comp)) +
                  " (" +
                  str(abs(fairways_hit_percent_comp)) +
                  f"%){Style.RESET_ALL}")
        elif fairways_hit_percent_comp < (PGA_FAIRWAYS_CLOSE_RANGE * -100):
            data.append(f"{Fore.RED}-" +
                  str(abs(fairways_hit_comp)) +
                  " (" +
                  str(abs(fairways_hit_percent_comp)) +
                  f"%){Style.RESET_ALL} ")
        else:
            data.append(f"{Fore.YELLOW}-" +
                  str(abs(fairways_hit_comp)) +
                  " (" +
                  str(abs(fairways_hit_percent_comp)) +
                  f"%){Style.RESET_ALL}")

        if greens_in_regulation_percent_comp >= 0:
            data.append(f"{Fore.GREEN}+" +
                  str(abs(greens_in_regulation_comp)) +
                  " (" +
                  str(abs(greens_in_regulation_percent_comp)) +
                  f"%){Style.RESET_ALL}")
        elif greens_in_regulation_percent_comp < (PGA_GIR_CLOSE_RANGE * -100):
            data.append(f"{Fore.RED}-" +
                  str(abs(greens_in_regulation_comp)) +
                  " (" +
                  str(abs(greens_in_regulation_percent_comp)) +
                  f"%){Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}-" +
                  str(abs(greens_in_regulation_comp)) +
                  " (" +
                  str(abs(greens_in_regulation_percent_comp)) +
                  f"%){Style.RESET_ALL}")
        if scrambling_comp is None:
            data.append(f"{Fore.RED}No data{Style.RESET_ALL}")
        elif scrambling_percent_comp is None:
            data.append(f"{Fore.RED}No data{Style.RESET_ALL}")
        else:
            if scrambling_percent_comp >= 0:
                data.append(f"{Fore.GREEN}+" +
                      str(abs(scrambling_comp)) +
                      " (" +
                      str(abs(scrambling_percent_comp)) +
                      f"%){Style.RESET_ALL}")
            elif scrambling_percent_comp < (PGA_SCRAMBLING_CLOSE_RANGE * -100):
                data.append(f"{Fore.RED}-" +
                      str(abs(scrambling_comp)) +
                      " (" +
                      str(abs(scrambling_percent_comp)) +
                      f"%){Style.RESET_ALL}")
            else:
                data.append(f"{Fore.RED}-" +
                      str(abs(scrambling_comp)) +
                      " (" +
                      str(abs(scrambling_percent_comp)) +
                      f"%){Style.RESET_ALL}")

        if putts_comp <= 0:
            data.append(f"{Fore.GREEN}-" +
                  str(abs(putts_comp)) +
                  f"{Style.RESET_ALL}")
        elif putts_comp > PGA_PUTTS_CLOSE_RANGE:
            data.append(f"{Fore.RED}+" +
                  str(abs(putts_comp)) +
                  f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}+" +
                  str(abs(putts_comp)) +
                  f"{Style.RESET_ALL}")

        if score_comp <= 0:
            data.append(f"{Fore.GREEN}-" + str(
                abs(score_comp)) + f"{Style.RESET_ALL}")
        elif score_comp > PGA_SCORING_CLOSE_RANGE:
            data.append(f"{Fore.RED}+" + str(
                abs(score_comp)) + f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}+" + str(
                abs(score_comp)) + f"{Style.RESET_ALL}")

        return data
    
    def print_stats_str(self):
        """
        Prints all the user stats
        """

        data = []

        data.append(self.total_rounds)

        if self.scoring_average <= PGA_SCORING_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")
        elif self.scoring_average > (PGA_SCORING_AVG + PGA_SCORING_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.scoring_average) +
                  f"{Style.RESET_ALL}")

        if round(
                self.fairways_hit /
                self.fairways_possible,
                2) >= PGA_FAIRWAYS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.GREEN}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")
        elif round(self.fairways_hit / self.fairways_possible, 2) < (PGA_FAIRWAYS_AVG - PGA_FAIRWAYS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.RED}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.fairways_hit) +
                  "/" +
                  str(self.fairways_possible) +
                  f"{Style.RESET_ALL} ({Fore.YELLOW}" +
                  str(self.fairways_hit_percentage) +
                  f"%{Style.RESET_ALL})")

        if round(self.greens_in_regulation /
                 (18 * self.total_rounds), 2) >= PGA_GIR_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.greens_in_regulation) +
                  "/" +
                  str(18 *
                      self.total_rounds) +
                  f"{Style.RESET_ALL} ({Fore.GREEN}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        elif round(self.greens_in_regulation / (18 * self.total_rounds), 2) < (PGA_GIR_AVG - PGA_GIR_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.greens_in_regulation) +
                  "/" +
                  str(18 *
                      self.total_rounds) +
                  f"{Style.RESET_ALL} ({Fore.RED}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.greens_in_regulation) +
                  "/" +
                  str(18 *
                      self.total_rounds) +
                  f"{Style.RESET_ALL} ({Fore.YELLOW}" +
                  str(self.greens_in_regulation_percentage) +
                  f"%{Style.RESET_ALL})")
        if self.scrambling_percentage is None:
            data.append(f"{Fore.RED}No data{Style.RESET_ALL}")
        else:
            if round(self.up_and_downs / ((18 * self.total_rounds) -
                                          self.greens_in_regulation), 2) >= PGA_SCRAMBLING_AVG:
                data.append(f"{Fore.GREEN}" +
                      str(self.up_and_downs) +
                      "/" +
                      str((18 *
                           self.total_rounds) -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.GREEN}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")
            elif round(self.up_and_downs / ((18 * self.total_rounds) - self.greens_in_regulation), 2) < (
                    PGA_SCRAMBLING_AVG - PGA_SCRAMBLING_CLOSE_RANGE):
                data.append(f"{Fore.RED}" +
                      str(self.up_and_downs) +
                      "/" +
                      str((18 *
                           self.total_rounds) -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.RED}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")
            else:
                data.append(f"{Fore.RED}" +
                      str(self.up_and_downs) +
                      "/" +
                      str((18 *
                           self.total_rounds) -
                          self.greens_in_regulation) +
                      f"{Style.RESET_ALL} ({Fore.RED}" +
                      str(self.scrambling_percentage) +
                      f"%{Style.RESET_ALL})")

        if self.putts_per_round <= PGA_PUTTS_AVG:
            data.append(f"{Fore.GREEN}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL} ({Fore.GREEN}" +
                  str(self.putts_per_hole) +
                  f"{Style.RESET_ALL}/Hole)")
        elif self.putts_per_round > (PGA_PUTTS_AVG + PGA_PUTTS_CLOSE_RANGE):
            data.append(f"{Fore.RED}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL} ({Fore.RED}" +
                  str(self.putts_per_hole) +
                  f"{Style.RESET_ALL}/Hole)")
        else:
            data.append(f"{Fore.YELLOW}" +
                  str(self.putts_per_round) +
                  f"{Style.RESET_ALL} ({Fore.YELLOW}" +
                  str(self.putts_per_hole) +
                  f"{Style.RESET_ALL}/Hole)")
        
        return data

    def write_to_csv(self):
        """
        Saves data to file
        """

        header = [
            "Fairways Hit",
            "Fairways Possible",
            "Greens In Regulation",
            "Up & Downs",
            "Putts",
            "Score"]

        with open("albatross_data.csv", "w", newline="", encoding="utf-8") as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(header)

            for new_round in self.rounds:
                data = [
                    new_round.fairways_hit,
                    new_round.fairways_possible,
                    new_round.greens_in_regulation,
                    new_round.up_and_downs,
                    new_round.putts,
                    new_round.score]
                writer.writerow(data)


def add_round(player):
    """
    Creates a new round from user input
    """
    fairways_possible = int(input("\nFairways Possible: "))
    if fairways_possible not in range(0, 19):
        raise ValueError
    fairways_hit = int(
        input(
            "Fairways Hit (<=" + str(fairways_possible) + "): "))
    if fairways_hit not in range(0, fairways_possible + 1):
        raise ValueError
    greens_in_regulation = int(
        input("Greens In Regulation: "))
    if greens_in_regulation not in range(0, 19):
        raise ValueError
    up_and_downs = 0 if greens_in_regulation == 18 else int(
        input("Successful Up & Downs (<=" + str(18 - greens_in_regulation) + "): "))
    if up_and_downs not in range(
            0, 19 - greens_in_regulation):
        raise ValueError
    putts = int(input("Putts: "))
    if putts not in range(0, 55):
        raise ValueError
    score = int(input("Score: "))
    if score not in range(18, 150):
        raise ValueError

    new_round = Round(fairways_hit,
                      fairways_possible,
                      greens_in_regulation,
                      up_and_downs,
                      putts,
                      score,
                      len(player.rounds) + 1)
    player.update(new_round)
    player.write_to_csv()

    tableprint.table([player.rounds[-1].print_stats_str()], ROUND_HEADER, width=18, style="round")
    tableprint.table([player.print_stats_str()], PLAYER_HEADER, width=18, style="round")



def stats(player):
    """
    Print user stats
    """
    
    tableprint.table([player.print_stats_str()], PLAYER_HEADER, width=18, style="round")


def rounds(player):
    """
    Prints all round data
    """

    data = []
    for new_round in player.rounds:
        # new_round.print_stats_str()
        data.append(new_round.print_stats_str())
    tableprint.table(data, ROUND_HEADER, width=18, style="round")

def head_to_head(player):
    """
    Compares user data to selected model
    """

    tiger_woods_2000 = ModelProfile(
        "Tiger Woods 2000", 71.22, 75.15, 67.08, 28.76, 67.79)
    tiger_woods_2006 = ModelProfile(
        "Tiger Woods 2006", 60.71, 74.15, 62.81, 29.38, 68.12)
    jordan_spieth_2015 = ModelProfile(
        "Jordan Spieth 2015", 62.91, 67.87, 65.03, 27.82, 68.93)
    vijay_singh_2004 = ModelProfile(
        "Vijay Singh 2004", 60.36, 73.03, 62.36, 29.24, 68.84)
    rory_mcilroy_2014 = ModelProfile(
        "Rory McIlroy 2014", 59.93, 69.44, 58.52, 28.59, 68.83)
    brooks_koepka_2018 = ModelProfile(
        "Brooks Koepka 2018", 56.85, 68.28, 63.28, 28.76, 69.44)

    while True:
        
        data = [
            tiger_woods_2000.print_stats_str(),
            vijay_singh_2004.print_stats_str(),
            tiger_woods_2006.print_stats_str(),
            rory_mcilroy_2014.print_stats_str(),
            jordan_spieth_2015.print_stats_str(),
            brooks_koepka_2018.print_stats_str()        
        ]
        
        for i in range(len(data)):
            data[i][0] = data[i][0] + f" {Fore.MAGENTA}(" + str(i+1) + f"){Style.RESET_ALL}"

        tableprint.table(data, MODEL_HEADER, width=22, style="round")

        model_option = input("\nSelect a Player/Season: ")
        if model_option == "1":
            tableprint.table([player.print_comp_str(tiger_woods_2000)], COMPARE_HEADER, width=18, style="round")
            break
        elif model_option == "2":
            tableprint.table([player.print_comp_str(tiger_woods_2006)], COMPARE_HEADER, width=18, style="round")
            break
        elif model_option == "3":
            tableprint.table([player.print_comp_str(jordan_spieth_2015)], COMPARE_HEADER, width=18, style="round")
            break
        elif model_option == "4":
            tableprint.table([player.print_comp_str(vijay_singh_2004)], COMPARE_HEADER, width=18, style="round")
            break
        elif model_option == "5":
            tableprint.table([player.print_comp_str(rory_mcilroy_2014)], COMPARE_HEADER, width=18, style="round")
            break
        elif model_option == "6":
            tableprint.table([player.print_comp_str(brooks_koepka_2018)], COMPARE_HEADER, width=18, style="round")
            break
        else:
            print(
                f"\n{Fore.RED}Please input a valid option{Style.RESET_ALL}")


def banner():
    """
    Prints ALbatross banner
    """
    print(
        f"\n{Fore.BLUE}______________________________________________________________________________________________\
{Style.RESET_ALL}")
    print(f"""{Fore.BLUE}
\t █████╗ ██╗     ██████╗  █████╗ ████████╗██████╗  ██████╗ ███████╗███████╗
\t██╔══██╗██║     ██╔══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔═══██╗██╔════╝██╔════╝
\t███████║██║     ██████╔╝███████║   ██║   ██████╔╝██║   ██║███████╗███████╗
\t██╔══██║██║     ██╔══██╗██╔══██║   ██║   ██╔══██╗██║   ██║╚════██║╚════██║
\t██║  ██║███████╗██████╔╝██║  ██║   ██║   ██║  ██║╚██████╔╝███████║███████║
\t╚═╝  ╚═╝╚══════╝╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝{Style.RESET_ALL}""")
    print(
        f"\n{Fore.GREEN}\t\t\t   -</Golf Statistical Analyzer/>-{Style.RESET_ALL}")
    print(
        f"\n{Fore.BLUE}______________________________________________________________________________________________\
{Style.RESET_ALL}")


def main():
    """
    Runs the program
    """
    banner()

    player = PlayerProfile()

    while True:
        try:
            option_string = f"\n{Fore.MAGENTA}(A){Style.RESET_ALL}dd New Round, {Fore.MAGENTA}(Q){Style.RESET_ALL}uit: "
            initial = True
            if len(player.rounds) > 0:
                option_string = f"\n{Fore.MAGENTA}(A){Style.RESET_ALL}dd New Round, {Fore.MAGENTA}(R){Style.RESET_ALL}ound History, {Fore.MAGENTA}(S){Style.RESET_ALL}tats, {Fore.MAGENTA}(H){Style.RESET_ALL}ead-to-Head PGA Tour, {Fore.MAGENTA}(Q){Style.RESET_ALL}uit: "
                initial = False

            option = input(option_string)

            if option in ["a", "A"]:
                while True:
                    try:
                        add_round(player)
                        break
                    except ValueError:
                        print(
                            f"\n{Fore.RED}Please input a valid value{Style.RESET_ALL}")
            elif option in ["r", "R"] and not initial:
                rounds(player)
            elif option in ["s", "S"] and not initial:
                stats(player)
            elif option in ["h", "H"] and not initial:
                head_to_head(player)
            elif option in ["q", "Q"]:
                print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}")
                sys.exit()
            else:
                print(f"\n{Fore.RED}Please input a valid option{Style.RESET_ALL}")
        except KeyboardInterrupt:
            print(f"\n\n{Fore.RED}Exiting Albatross{Style.RESET_ALL}")
            sys.exit()


if __name__ == "__main__":
    main()
