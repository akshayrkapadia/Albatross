from colorama import Fore, Style


def validateType(value: int | float | bool | str, expectedType: type) -> bool:
    """
    Checks if the value matches the expected type
    """

    if type(value) is not expectedType:
        raise TypeError(
            f"\n\n{Fore.RED}Type Error: Value Type Does Not Match Expected Type{Style.RESET_ALL}\n")
    else:
        return True


def validateValue(value: int | float | bool | str, expectedLow: int | float, expectedHigh: int | float) -> bool:
    """
    Checks if the value is within the acceptable range
    """

    if (value >= expectedLow) and (value <= expectedHigh):
        return True
    else:
        raise ValueError(
            f"\n\n{Fore.RED}Value Error: Value Out Of Bounds{Style.RESET_ALL}\n")


def formatString(value: int | float, targetValue: int | float, closeRange: int | float, outputString: str = "", reverse: bool = False) -> str:
    """ 
    Returns a color coded formatted string for the given stat
    """

    if outputString == "":
        outputString = str(value)
    if reverse:
        if value == -1:
            return "-"
        elif value == targetValue:
            return str(outputString)
        elif value <= targetValue:
            return f"{Fore.GREEN}" + str(outputString) + f"{Style.RESET_ALL}"
        elif value > (targetValue + closeRange):
            return f"{Fore.RED}" + str(outputString) + f"{Style.RESET_ALL}"
        else:
            return f"{Fore.YELLOW}" + str(outputString) + f"{Style.RESET_ALL}"
    else:
        if value == -1:
            return "-"
        elif value == targetValue:
            return str(outputString)
        elif value >= targetValue:
            return f"{Fore.GREEN}" + str(outputString) + f"{Style.RESET_ALL}"
        elif value < (targetValue - closeRange):
            return f"{Fore.RED}" + str(outputString) + f"{Style.RESET_ALL}"
        else:
            return f"{Fore.YELLOW}" + str(outputString) + f"{Style.RESET_ALL}"
