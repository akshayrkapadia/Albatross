from Functions import formatString
from Constants import *


class Round:
    """
    Class for holding data for a round
    """

    def __init__(self, date: str, courseID: str, round_number: int, holes: int) -> None:
        self.date = date
        self.courseID = courseID
        self.holes = holes
        self.round_number = round_number
        self.par = 0
        self.score = 0
        self.fairways_hit = 0
        self.fairways_possible = 0
        self.greens_in_regulation = 0
        self.putts = 0
        self.putts_on_gir = 0
        self.putts_per_gir = 0
        self.scrambles = 0
        self.sand_saves_possible = 0
        self.sand_saves = 0
        self.penalty_shots = 0

        for hole in holes:
            self.par += hole.par
            self.score += hole.score
            if hole.par > 3:
                self.fairways_possible += 1
                if hole.fairway:
                    self.fairways_hit += 1
            if hole.green_in_regulation:
                self.greens_in_regulation += 1
                self.putts_on_gir += hole.putts
            self.putts += hole.putts
            if hole.scramble > -1:
                self.scrambles += hole.scramble
            if hole.sand_save > -1:
                self.sand_saves_possible += 1
                if hole.sand_save:
                    self.sand_saves += 1
            self.penalty_shots += hole.penalty_shots
        self.putts_per_gir = round(
            self.putts_on_gir / self.greens_in_regulation, 2)

        self.fairways_hit_percentage = -1 if not self.fairways_possible else round(
            (self.fairways_hit / self.fairways_possible) * 100, 2)
        self.greens_in_regulation_percentage = round(
            (self.greens_in_regulation / 18) * 100, 2)
        self.scrambling_percentage = -1 if (self.greens_in_regulation == 18) else round(
            ((self.scrambles / (18 - self.greens_in_regulation)) * 100), 2)
        self.sand_saves_percentage = -1
        if self.sand_saves_possible > 0:
            self.sand_saves_percentage = round(
                (self.sand_saves / self.sand_saves_possible) * 100, 2)

    # TODO for v0.0.2
    # def getScoreBreakdown(self) -> str:

        # birdieOrBetter = ""
        # pars = ""
        # bogeyOrWorse = ""

        # for hole in self.holes:
        #     if hole.score < hole.par:
        #         birdieOrBetter += f"{Fore.GREEN}-{Style.RESET_ALL}"
        #     elif hole.score > hole.par:
        #         bogeyOrWorse += f"{Fore.RED}-{Style.RESET_ALL}"
        #     else:
        #         pars += "-"

        # breakdown = birdieOrBetter + pars + bogeyOrWorse

        # breakdown = ""

        # for hole in self.holes:
        #     delta = hole.score - hole.par
        #     if hole.score < hole.par:
        #         breakdown += f"{Fore.GREEN}" + \
        #             str(delta) + f"{Style.RESET_ALL}"
        #     elif hole.score > hole.par:
        #         breakdown += f"{Fore.RED}+" + str(delta) + f"{Style.RESET_ALL}"
        #     else:
        #         breakdown += "-"

        # return breakdown

    def getStatsForTable(self, includeCourseID: bool = True) -> list:
        """
        Returns all the stats needed for round stats
        """

        data = []

        if includeCourseID:
            data = [
                self.date,
                self.courseID,
                self.par,
                formatString(self.score, PGA_SCORING_AVG, PGA_SCORING_CLOSE_RANGE,
                             outputString=str(self.score) + " (" + str(self.score-self.par) + ")" if self.score < self.par
                             else str(self.score) + " (+" + str(self.score-self.par) + ")" if self.score > self.par
                             else str(self.score) + " (E)", reverse=True),
                formatString(self.fairways_hit_percentage, PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                             outputString=str(self.fairways_hit) + "/" + str(self.fairways_possible) + " (" + str(self.fairways_hit_percentage) + "%)"),
                formatString(self.greens_in_regulation_percentage, PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                             outputString=str(self.greens_in_regulation) + "/18 (" + str(self.greens_in_regulation_percentage) + "%)"),
                formatString(self.putts, PGA_PUTTS_AVG, PGA_PUTTS_CLOSE_RANGE, reverse=True,
                             outputString=str(self.putts) + formatString(self.putts_per_gir, PGA_PUTTS_PER_GIR, PGA_PUTTS_PER_GIR_CLOSE_RANGE,
                                                                         reverse=True, outputString=" (" + str(self.putts_per_gir) + "/GIR)")),
                formatString(self.scrambling_percentage, PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                             outputString=str(self.scrambles) + "/" + str(18 - self.greens_in_regulation) + " (" + str(self.scrambling_percentage) + "%)"),
                formatString(self.sand_saves_percentage, PGA_SAND_SAVE_AVG, PGA_SAND_SAVE_CLOSE_RANGE,
                             outputString=str(self.sand_saves) + "/" + str(self.sand_saves_possible) + " (" + str(self.sand_saves_percentage) + "%)"),
                formatString(
                    self.penalty_shots, PGA_PENALTY_SHOTS_AVG, PGA_PENALTY_SHOTS_CLOSE_RANGE, reverse=True),
            ]

        else:
            data = [
                self.date,
                self.par,
                formatString(self.score, PGA_SCORING_AVG, PGA_SCORING_CLOSE_RANGE,
                             outputString=str(self.score) + " (" + str(self.score-self.par) + ")" if self.score < self.par
                             else str(self.score) + " (+" + str(self.score-self.par) + ")" if self.score > self.par
                             else str(self.score) + " (E)", reverse=True),
                formatString(self.fairways_hit_percentage, PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                             outputString=str(self.fairways_hit) + "/" + str(self.fairways_possible) + " (" + str(self.fairways_hit_percentage) + "%)"),
                formatString(self.greens_in_regulation_percentage, PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                             outputString=str(self.greens_in_regulation) + "/18 (" + str(self.greens_in_regulation_percentage) + "%)"),
                formatString(self.putts, PGA_PUTTS_AVG, PGA_PUTTS_CLOSE_RANGE, reverse=True,
                             outputString=str(self.putts) + formatString(self.putts_per_gir, PGA_PUTTS_PER_GIR, PGA_PUTTS_PER_GIR_CLOSE_RANGE,
                                                                         reverse=True, outputString=" (" + str(self.putts_per_gir) + "/GIR)")),
                formatString(self.scrambling_percentage, PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                             outputString=str(self.scrambles) + "/" + str(18 - self.greens_in_regulation) + " (" + str(self.scrambling_percentage) + "%)"),
                formatString(self.sand_saves_percentage, PGA_SAND_SAVE_AVG, PGA_SAND_SAVE_CLOSE_RANGE,
                             outputString=str(self.sand_saves) + "/" + str(self.sand_saves_possible) + " (" + str(self.sand_saves_percentage) + "%)"),
                formatString(
                    self.penalty_shots, PGA_PENALTY_SHOTS_AVG, PGA_PENALTY_SHOTS_CLOSE_RANGE, reverse=True),
            ]

        return data

    def getHoleByHoleStatsForTable(self) -> list:
        """
        Returns all the stats needed for scorecard
        """

        data = []

        for hole in self.holes:
            data.append(hole.getStatsForTable())

        roundStats = self.getStatsForTable(includeCourseID=False)
        roundStats[0] = "Total"
        data.append(roundStats)

        return data

    def getHolesStatsForCSV(self) -> list:
        """
        Returns all the stats that will be saved to CSV file
        """

        data = []

        for hole in self.holes:
            data.append(hole.getStatsForCSV())

        return data
