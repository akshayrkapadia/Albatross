from CourseTemplate import CourseTemplate
from ModelProfile import ModelProfile
from Functions import formatString
from datetime import datetime
from Round import Round
from Constants import *
from Hole import Hole
import csv
import os


class PlayerProfile:
    """
    Class to hold user data
    """

    def __init__(self) -> None:
        self.fairways_hit = 0
        self.fairways_possible = 0
        self.fairways_hit_percentage = -1
        self.greens_in_regulation = 0
        self.greens_in_regulation_percentage = 0
        self.scrambles = 0
        self.scrambling_percentage = -1
        self.sand_saves_possible = 0
        self.sand_saves = 0
        self.sand_saves_percentage = -1
        self.putts = 0
        self.putts_per_round = 0
        self.putts_per_hole = 0
        self.putts_per_gir = 0
        self.putts_on_gir = 0
        self.scoring_average = 0
        self.penalty_shots = 0
        self.penalty_shots_per_round = 0
        self.total_rounds = 0
        self.rounds = []
        self.courseTemplates = []

        homeDir = os.path.expanduser('~')
        dataFile = os.path.join(
            homeDir, ".Albatross", "albatross_data.csv")

        if os.path.isfile(dataFile):
            with open(dataFile, "r+", newline="", encoding="utf-8") as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=",")
                line_count = 0
                round_number = 0
                holes = []
                firstLine = True
                date = ""
                courseID = ""
                for row in csv_reader:
                    if line_count > 0:
                        if firstLine:
                            date = row[0]
                            courseID = row[1]
                            firstLine = False
                        elif row[0] == "$":
                            round_number += 1
                            new_round = Round(
                                date, courseID, round_number, holes)
                            self.update(new_round)
                            holes = []
                            firstLine = True
                        else:
                            hole_number = int(row[0])
                            par = int(row[1])
                            score = int(row[2])
                            fairway = int(row[3])
                            gir = int(row[4])
                            putts = int(row[5])
                            scramble = int(row[6])
                            sand_save = int(row[7])
                            penalty_shots = int(row[8])

                            holes.append(Hole(
                                hole_number,
                                par,
                                score,
                                fairway,
                                gir,
                                putts,
                                scramble,
                                sand_save,
                                penalty_shots
                            ))
                    line_count += 1

    def update(self, new_round: Round) -> None:
        """
        Updates class fields with new round
        """

        if new_round.courseID not in [courseTemplate.courseID for courseTemplate in self.courseTemplates]:
            self.courseTemplates.append(CourseTemplate(
                new_round.courseID, [hole.par for hole in new_round.holes]))

        self.rounds.append(new_round)
        self.rounds.sort(key=lambda new_round: datetime.strptime(
            new_round.date, '%m/%d/%Y'))

        self.total_rounds += 1

        if new_round.fairways_hit_percentage > -1:
            self.fairways_hit += new_round.fairways_hit
            self.fairways_possible += new_round.fairways_possible
            self.fairways_hit_percentage = round(
                (self.fairways_hit / self.fairways_possible) * 100, 2)

        self.greens_in_regulation += new_round.greens_in_regulation
        self.greens_in_regulation_percentage = round(
            (self.greens_in_regulation / (self.total_rounds * 18)) * 100, 2)

        if (new_round.scrambling_percentage > -1) and (new_round.greens_in_regulation < 18):
            self.scrambles += new_round.scrambles
            self.scrambling_percentage = round(
                ((self.scrambles / ((self.total_rounds * 18) - self.greens_in_regulation)) * 100), 2)

        if (new_round.sand_saves_percentage > -1) and (new_round.greens_in_regulation < 18):
            self.sand_saves_possible += new_round.sand_saves_possible
            self.sand_saves = new_round.sand_saves
            self.sand_saves_percentage = round(
                ((self.sand_saves / self.sand_saves_possible) * 100), 2)

        self.putts += new_round.putts
        self.putts_per_round = round(
            ((self.putts_per_round * (self.total_rounds - 1)) + new_round.putts) / self.total_rounds, 2)
        self.putts_per_hole = round(self.putts_per_round / 18, 2)
        self.putts_on_gir += new_round.putts_on_gir
        self.putts_per_gir = self.putts_per_gir if new_round.greens_in_regulation == 0 else round(
            self.putts_on_gir / self.greens_in_regulation, 2)

        self.scoring_average = round(
            ((self.scoring_average * (self.total_rounds - 1)) + new_round.score) / self.total_rounds, 2)

        self.penalty_shots += new_round.penalty_shots
        self.penalty_shots_per_round = round(
            self.penalty_shots / self.total_rounds, 2)

    def deleteRound(self, roundNumberIndex: int) -> None:
        """
        Deletes the round from the list at the given index
        """

        del self.rounds[roundNumberIndex]

    def getRoundsByCourseID(self, courseID: str, rounds: bool = False) -> list[Round]:
        """
        Returns all rounds that match the given courseID
        """

        if not rounds:
            rounds = self.rounds
        return [new_round for new_round in rounds if (new_round.courseID == courseID)]

    def getRoundsByRecency(self, numRounds: int, rounds: bool = False) -> list[Round]:
        """
        Returns the X most recent rounds
        """

        if not rounds:
            rounds = self.rounds
        return rounds[-numRounds:]

    def calcFilteredStats(self, filteredRounds: list[Round]) -> list[float]:
        """
        Returns all stats for the given rounds
        """

        fairways_hit = 0
        fairways_possible = 0
        fairways_hit_percentage = -1
        greens_in_regulation = 0
        greens_in_regulation_percentage = 0
        scrambles = 0
        scrambling_percentage = -1
        sand_saves_possible = 0
        sand_saves = 0
        sand_saves_percentage = -1
        putts = 0
        putts_per_round = 0
        putts_per_hole = 0
        putts_per_gir = 0
        putts_on_gir = 0
        scoring_average = 0
        penalty_shots = 0
        penalty_shots_per_round = 0
        total_rounds = 0

        for new_round in filteredRounds:

            total_rounds += 1

            if new_round.fairways_hit_percentage > -1:
                fairways_hit += new_round.fairways_hit
                fairways_possible += new_round.fairways_possible
                fairways_hit_percentage = round(
                    (fairways_hit / fairways_possible) * 100, 2)

            greens_in_regulation += new_round.greens_in_regulation
            greens_in_regulation_percentage = round(
                (greens_in_regulation / (total_rounds * 18)) * 100, 2)

            if (new_round.scrambling_percentage > -1) and (new_round.greens_in_regulation < 18):
                scrambles += new_round.scrambles
                scrambling_percentage = round(
                    ((scrambles / ((total_rounds * 18) - greens_in_regulation)) * 100), 2)

            if (new_round.sand_saves_percentage > -1) and (new_round.greens_in_regulation < 18):
                sand_saves_possible += new_round.sand_saves_possible
                sand_saves = new_round.sand_saves
                sand_saves_percentage = round(
                    ((sand_saves / sand_saves_possible) * 100), 2)

            putts += new_round.putts
            putts_per_round = round(
                ((putts_per_round * (total_rounds - 1)) + new_round.putts) / total_rounds, 2)
            # putts_per_hole = round(putts_per_round / 18, 2)
            putts_on_gir += new_round.putts_on_gir
            putts_per_gir = putts_per_gir if new_round.greens_in_regulation == 0 else round(
                putts_on_gir / greens_in_regulation, 2)

            scoring_average = round(
                ((scoring_average * (total_rounds - 1)) + new_round.score) / total_rounds, 2)

            penalty_shots += new_round.penalty_shots
            penalty_shots_per_round = round(penalty_shots / total_rounds, 2)

        return [
            total_rounds,
            scoring_average,
            fairways_hit_percentage,
            greens_in_regulation_percentage,
            scrambling_percentage,
            sand_saves_percentage,
            putts_per_round,
            penalty_shots_per_round,
            fairways_hit,
            fairways_possible,
            greens_in_regulation,
            scrambles,
            sand_saves,
            sand_saves_possible,
            putts_per_gir
        ]

    def getStatsForTable(self, filteredRounds: bool = False) -> list[str]:
        """
        Returns all user stats
        """

        total_rounds = self.total_rounds
        scoring_average = self.scoring_average
        fairways_hit_percentage = self.fairways_hit_percentage
        greens_in_regulation_percentage = self.greens_in_regulation_percentage
        scrambling_percentage = self.scrambling_percentage
        sand_saves_percentage = self.sand_saves_percentage
        putts_per_round = self.putts_per_round
        penalty_shots_per_round = self.penalty_shots_per_round
        fairways_hit = self.fairways_hit
        fairways_possible = self.fairways_possible
        greens_in_regulation = self.greens_in_regulation
        scrambles = self.scrambles
        sand_saves = self.sand_saves
        sand_saves_possible = self.sand_saves_possible
        putts_per_gir = self.putts_per_gir

        if filteredRounds:
            filteredStats = self.calcFilteredStats(filteredRounds)
            total_rounds = filteredStats[0]
            scoring_average = filteredStats[1]
            fairways_hit_percentage = filteredStats[2]
            greens_in_regulation_percentage = filteredStats[3]
            scrambling_percentage = filteredStats[4]
            sand_saves_percentage = filteredStats[5]
            putts_per_round = filteredStats[6]
            penalty_shots_per_round = filteredStats[7]
            fairways_hit = filteredStats[8]
            fairways_possible = filteredStats[9]
            greens_in_regulation = filteredStats[10]
            scrambles = filteredStats[11]
            sand_saves = filteredStats[12]
            sand_saves_possible = filteredStats[13]
            putts_per_gir = filteredStats[14]

        data = [
            total_rounds,
            formatString(
                scoring_average, PGA_SCORING_AVG, PGA_SCORING_CLOSE_RANGE, reverse=True),
            formatString(fairways_hit_percentage, PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                         outputString=str(fairways_hit) + "/" + str(fairways_possible)),
            formatString(greens_in_regulation_percentage, PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                         outputString=str(greens_in_regulation) + "/" + str(18 * total_rounds)),
            formatString(scrambling_percentage, PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                         outputString=str(scrambles) + "/" + str((18 * total_rounds) - greens_in_regulation)),
            formatString(sand_saves_percentage, PGA_SAND_SAVE_AVG, PGA_SAND_SAVE_CLOSE_RANGE,
                         outputString=str(sand_saves) + "/" + str(sand_saves_possible)),
            formatString(putts_per_round, PGA_PUTTS_AVG, PGA_PUTTS_CLOSE_RANGE, reverse=True,
                         outputString=str(putts_per_round) + " Putts"),
            formatString(penalty_shots_per_round, PGA_PENALTY_SHOTS_AVG, PGA_PENALTY_SHOTS_CLOSE_RANGE, reverse=True,
                         outputString=str(penalty_shots_per_round) + "/Round")
        ]

        extraData = [
            "",
            "",
            formatString(fairways_hit_percentage, PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                         outputString=str(fairways_hit_percentage) + "%"),
            formatString(greens_in_regulation_percentage, PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                         outputString=str(greens_in_regulation_percentage) + "%"),
            formatString(scrambling_percentage, PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                         outputString=str(scrambling_percentage) + "%"),
            formatString(sand_saves_percentage, PGA_SAND_SAVE_AVG, PGA_SAND_SAVE_CLOSE_RANGE,
                         outputString=str(sand_saves_percentage) + "%"),
            formatString(putts_per_gir, PGA_PUTTS_PER_GIR, PGA_PUTTS_PER_GIR_CLOSE_RANGE, reverse=True,
                         outputString=str(putts_per_gir) + "/GIR"),
            ""
        ]

        return data, extraData

    def getHoleByHoleStatsForTable(self, roundNumberIndex: int) -> list[str]:
        """
        Returns hole-by-hole stats for the given round
        """

        return self.rounds[roundNumberIndex].getHoleByHoleStatsForTable()

    def getCourseStatsForTable(self, courseID: str) -> (list[str], list[str]):
        """
        Returns course stats for the given course
        """

        front9 = []
        back9 = []
        numRounds: int = 0
        holes: dict = {}

        for roundIndex in range(len(self.rounds)):
            new_round = self.rounds[roundIndex]
            if new_round.courseID == courseID:
                numRounds += 1
                for holeIndex in range(18):
                    hole = new_round.holes[holeIndex]

                    if holeIndex in holes.keys():
                        holes[holeIndex]["Score"] += hole.score
                        holes[holeIndex]["Fairways"] += 1 if hole.fairway > 0 else 0
                        holes[holeIndex]["Fairways Possible"] += 1 if hole.fairway >= 0 else 0
                        holes[holeIndex]["GIR"] += hole.green_in_regulation
                        holes[holeIndex]["Scramble"] += 1 if hole.scramble > 0 else 0
                        holes[holeIndex]["Scrambles Possible"] += 1 if hole.scramble >= 0 else 0
                        holes[holeIndex]["Sand Save"] += 1 if hole.sand_save > 0 else 0
                        holes[holeIndex]["Sand Saves Possible"] += 1 if hole.sand_save >= 0 else 0
                        holes[holeIndex]["Putts"] += hole.putts
                        holes[holeIndex]["Penalty"] += hole.penalty_shots
                    else:
                        holes[holeIndex] = {
                            "Par": hole.par,
                            "Score": hole.score,
                            "Fairways": 1 if hole.fairway > 0 else 0,
                            "Fairways Possible": 1 if hole.fairway >= 0 else 0,
                            "GIR": hole.green_in_regulation,
                            "Scramble": 1 if hole.scramble > 0 else 0,
                            "Scrambles Possible": 1 if hole.scramble >= 0 else 0,
                            "Sand Save": 1 if hole.sand_save > 0 else 0,
                            "Sand Saves Possible": 1 if hole.sand_save >= 0 else 0,
                            "Putts": hole.putts,
                            "Penalty": hole.penalty_shots
                        }

        pars = [holes[holeIndex]["Par"] for holeIndex in range(18)]
        front9ParTotal = sum(pars[:9])
        back9ParTotal = sum(pars[9:18])

        scores = [round(holes[holeIndex]["Score"] /
                  numRounds, 1) for holeIndex in range(18)]
        formattedScores = [formatString(
            scores[holeIndex], pars[holeIndex], 0.5, reverse=True) for holeIndex in range(18)]
        formattedFront9TotalScore = formatString(
            round(sum(scores[:9]), 1), front9ParTotal, 2, reverse=True)
        formattedBack9TotalScore = formatString(
            round(sum(scores[9:18]), 1), back9ParTotal, 2, reverse=True)

        fairways = [round((holes[holeIndex]["Fairways"] / numRounds) * 100, 2)
                    if holes[holeIndex]["Par"] > 3 else -1 for holeIndex in range(18)]
        formattedFairways = [formatString(fairways[holeIndex], PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                                          outputString=str(holes[holeIndex]["Fairways"]) + "/" + str(holes[holeIndex]["Fairways Possible"])) for holeIndex in range(18)]

        formattedFairwaysFront9Total = formatString(round(sum(fairways[:9]) / 9, 2), PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                                                    outputString=str(sum([holes[holeIndex]["Fairways"] for holeIndex in range(9)])) + "/" + str(sum([holes[holeIndex]["Fairways Possible"] for holeIndex in range(9)])))

        formattedFairwaysBack9Total = formatString(round(sum(fairways[9:18]) / 9, 2), PGA_FAIRWAYS_AVG, PGA_FAIRWAYS_CLOSE_RANGE,
                                                   outputString=str(sum([holes[holeIndex]["Fairways"] for holeIndex in range(9, 18)])) + "/" + str(sum([holes[holeIndex]["Fairways Possible"] for holeIndex in range(9, 18)])))

        girs = [round((holes[holeIndex]["GIR"]/numRounds) * 100, 2)
                for holeIndex in range(18)]
        formattedGIRs = [formatString(round(girs[holeIndex], 2), PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                                      outputString=str(holes[holeIndex]["GIR"]) + "/" + str(numRounds)) for holeIndex in range(18)]
        formattedFront9GIRs = formatString(round(sum(girs[:9]), 2) / 9, PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                                           outputString=str(sum([holes[holeIndex]["GIR"] for holeIndex in range(9)])) + "/" + str(numRounds * 9))
        formattedBack9GIRs = formatString(round(sum(girs[9:18]) / 9, 2), PGA_GIR_AVG, PGA_GIR_CLOSE_RANGE,
                                          outputString=str(sum([holes[holeIndex]["GIR"] for holeIndex in range(9, 18)])) + "/" + str(numRounds * 9))

        scrambles = [round((holes[holeIndex]["Scramble"]/holes[holeIndex]["Scrambles Possible"]) * 100, 2)
                     if holes[holeIndex]["Scrambles Possible"] > 0 else -1 for holeIndex in range(18)]
        formattedScrambles = [formatString(scrambles[holeIndex], PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                                           outputString=str(holes[holeIndex]["Scramble"]) + "/" + str(holes[holeIndex]["Scrambles Possible"])) for holeIndex in range(18)]
        formattedScramblesFront9Total = formatString(round(sum(scrambles[:9]) / 9, 2), PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                                                     outputString=str(sum([holes[holeIndex]["Scramble"] for holeIndex in range(9)])) + "/" + str(sum([holes[holeIndex]["Scrambles Possible"] for holeIndex in range(9)])))
        formattedScramblesBack9Total = formatString(round(sum(scrambles[9:18]) / 9, 2), PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                                                    outputString=str(sum([holes[holeIndex]["Scramble"] for holeIndex in range(9, 18)])) + "/" + str(sum([holes[holeIndex]["Scrambles Possible"] for holeIndex in range(9, 18)])))

        sandSaves = [round((holes[holeIndex]["Sand Save"]/holes[holeIndex]["Sand Saves Possible"]) * 100, 2)
                     if holes[holeIndex]["Sand Saves Possible"] > 0 else -1 for holeIndex in range(18)]
        formattedSandSaves = [formatString(sandSaves[holeIndex], PGA_SAND_SAVE_AVG, PGA_SAND_SAVE_CLOSE_RANGE,
                                           outputString=str(holes[holeIndex]["Sand Save"]) + "/" + str(holes[holeIndex]["Sand Saves Possible"])) for holeIndex in range(18)]
        formattedSandSavesFront9Total = formatString(round(sum(sandSaves[:9]) / 9, 2), PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                                                     outputString=str(sum([holes[holeIndex]["Sand Save"] for holeIndex in range(9)])) + "/" + str(sum([holes[holeIndex]["Sand Saves Possible"] for holeIndex in range(9)])))
        formattedSandSavesBack9Total = formatString(round(sum(sandSaves[9:18]) / 9, 2), PGA_SCRAMBLING_AVG, PGA_SCRAMBLING_CLOSE_RANGE,
                                                    outputString=str(sum([holes[holeIndex]["Sand Save"] for holeIndex in range(9, 18)])) + "/" + str(sum([holes[holeIndex]["Sand Saves Possible"] for holeIndex in range(9, 18)])))

        putts = [round(holes[holeIndex]["Putts"]/numRounds, 2)
                 for holeIndex in range(18)]
        formattedPutts = [formatString(
            putts[holeIndex], PGA_PUTTS_AVG/18, PGA_PUTTS_CLOSE_RANGE/18, reverse=True) for holeIndex in range(18)]
        formattedPuttsFront9Total = formatString(
            round(sum(putts[:9]), 2), PGA_PUTTS_AVG/2, PGA_PUTTS_CLOSE_RANGE/2, reverse=True)
        formattedPuttsBack9Total = formatString(round(
            sum(putts[9:18]), 2), PGA_PUTTS_AVG/2, PGA_PUTTS_CLOSE_RANGE/2, reverse=True)

        penalties = [holes[holeIndex]["Penalty"] for holeIndex in range(18)]
        formattedPenalties = [formatString(
            penalties[holeIndex], PGA_PENALTY_SHOTS_AVG/18, PGA_PENALTY_SHOTS_CLOSE_RANGE, reverse=True) for holeIndex in range(18)]
        formattedPenaltiesFront9Total = formatString(round(sum(
            penalties[:9]), 1), PGA_PENALTY_SHOTS_AVG/2, PGA_PENALTY_SHOTS_CLOSE_RANGE/2, reverse=True)
        formattedPenaltiesBack9Total = formatString(round(sum(
            penalties[9:18]), 1), PGA_PENALTY_SHOTS_AVG/2, PGA_PENALTY_SHOTS_CLOSE_RANGE/2, reverse=True)

        front9.append(["Par"] + pars[:9] + [front9ParTotal])
        front9.append(["Score"] + formattedScores[:9] +
                      [formattedFront9TotalScore])
        front9.append(["Fairways"] + formattedFairways[:9] +
                      [formattedFairwaysFront9Total])
        front9.append(["GIR"] + formattedGIRs[:9] + [formattedFront9GIRs])
        front9.append(["Putts"] + formattedPutts[:9] +
                      [formattedPuttsFront9Total])
        front9.append(["Scrambles"] + formattedScrambles[:9] +
                      [formattedScramblesFront9Total])
        front9.append(["Sand Saves"] + formattedSandSaves[:9] +
                      [formattedSandSavesFront9Total])
        front9.append(["Penalties"] + formattedPenalties[:9] +
                      [formattedPenaltiesFront9Total])

        back9.append(["Par"] + pars[9:18] + [back9ParTotal])
        back9.append(["Score"] + formattedScores[9:18] +
                     [formattedBack9TotalScore])
        back9.append(["Fairways"] + formattedFairways[9:18] +
                     [formattedFairwaysBack9Total])
        back9.append(["GIR"] + formattedGIRs[9:18] + [formattedBack9GIRs])
        back9.append(["Putts"] + formattedPutts[9:18] +
                     [formattedPuttsBack9Total])
        back9.append(["Scrambles"] + formattedScrambles[9:18] +
                     [formattedScramblesBack9Total])
        back9.append(["Sand Saves"] + formattedSandSaves[9:18] +
                     [formattedSandSavesBack9Total])
        back9.append(["Penalties"] + formattedPenalties[9:18] +
                     [formattedPenaltiesBack9Total])

        return front9, back9

    def getModelProfile(self):
        return ModelProfile("You", self.fairways_hit_percentage, self.greens_in_regulation_percentage,
                            self.scrambling_percentage, self.sand_saves_percentage, self.putts_per_round, self.scoring_average)

    def write_to_csv(self) -> None:
        """
        Saves data to file
        """

        homeDir = os.path.expanduser('~')
        dataDir = os.path.join(homeDir, ".Albatross")
        dataFile = os.path.join(dataDir, "albatross_data.csv")

        if not os.path.exists(dataDir):
            os.makedirs(dataDir)

        with open(dataFile, "w", newline="", encoding="utf-8") as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow(HOLE_HEADER)
            for new_round in self.rounds:
                writer.writerow([new_round.date, new_round.courseID])
                holes = new_round.getHolesStatsForCSV()
                for hole in holes:
                    writer.writerow(hole)
                writer.writerow("$")
