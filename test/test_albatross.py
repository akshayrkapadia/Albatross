from os.path import isfile
from os import remove
import pytest

import Albatross.src.albatross as albatross


@pytest.mark.parametrize("values", [
    [
        [12, 14, 85.71, 15, 83.33, 2, 66.67, 28, 28, 1.56, 67, 67, 1],
        [5, 15, 58.62, 18, 91.67, 0, 66.67, 25, 26.5, 1.47, 62, 64.5, 2]
    ],
    [
        [5, 15, 33.33, 18, 100.00, 0, None, 25, 25, 1.39, 62, 62, 1],
        [12, 14, 58.62, 15, 91.67, 2, 66.67, 28, 26.5, 1.47, 67, 64.5, 2]
    ]
])
def test_player(values):
    """
    Tests the PlayerProfile class
    """

    player1 = albatross.PlayerProfile()

    round1 = albatross.Round(
        values[0][0], values[0][1], values[0][3], values[0][5], values[0][7], values[0][10], values[0][12])

    player1.update(round1)

    assert player1.fairways_hit == values[0][0]
    assert player1.fairways_possible == values[0][1]
    assert player1.fairways_hit_percentage == values[0][2]
    assert player1.greens_in_regulation == values[0][3]
    assert player1.greens_in_regulation_percentage == values[0][4]
    assert player1.up_and_downs == values[0][5]
    assert player1.scrambling_percentage == values[0][6]
    assert player1.putts == values[0][7]
    assert player1.putts_per_round == values[0][8]
    assert player1.putts_per_hole == values[0][9]
    assert player1.scoring_average == values[0][11]
    assert player1.total_rounds == values[0][12]
    assert len(player1.rounds) == values[0][12]

    player1.write_to_csv()

    assert isfile("albatross_data.csv")

    round2 = albatross.Round(
        values[1][0],  values[1][1],  values[1][3],  values[1][5],  values[1][7], values[1][10], values[1][12])

    player1.update(round2)

    assert player1.fairways_hit == values[0][0] + values[1][0]
    assert player1.fairways_possible == values[0][1] + values[1][1]
    assert player1.fairways_hit_percentage == values[1][2]
    assert player1.greens_in_regulation == values[0][3] + values[1][3]
    assert player1.greens_in_regulation_percentage == values[1][4]
    assert player1.up_and_downs == values[0][5] + values[1][5]
    assert player1.scrambling_percentage == values[1][6]
    assert player1.putts == values[0][7] + values[1][7]
    assert player1.putts_per_round == values[1][8]
    assert player1.putts_per_hole == values[1][9]
    assert player1.scoring_average == values[1][11]
    assert player1.total_rounds == values[1][12]
    assert len(player1.rounds) == values[1][12]

    player1.write_to_csv()

    assert isfile("albatross_data.csv")

    if isfile("albatross_data.csv"):

        player2 = albatross.PlayerProfile()

        assert player2.fairways_hit == values[0][0] + values[1][0]
        assert player2.fairways_possible == values[0][1] + values[1][1]
        assert player2.fairways_hit_percentage == values[1][2]
        assert player2.greens_in_regulation == values[0][3] + values[1][3]
        assert player2.greens_in_regulation_percentage == values[1][4]
        assert player2.up_and_downs == values[0][5] + values[1][5]
        assert player2.scrambling_percentage == values[1][6]
        assert player2.putts == values[0][7] + values[1][7]
        assert player2.putts_per_round == values[1][8]
        assert player2.putts_per_hole == values[1][9]
        assert player2.scoring_average == values[1][11]
        assert player2.total_rounds == values[1][12]
        assert len(player2.rounds) == values[1][12]

        remove("albatross_data.csv")


@pytest.mark.parametrize("values", [
    [12, 14, 15, 2, 28, 67, 1],
    [5, 15, 18, 0, 25, 62, 2],
    [14, 14, 18, 0, 20, 59, 3],
    [0, 14, 0, 0, 25, 70, 4]
])
def test_round(values):
    """
    Tests the Round class
    """
    new_round = albatross.Round(
        values[0], values[1], values[2], values[3], values[4], values[5], values[6])

    assert new_round.fairways_hit == values[0]
    assert new_round.fairways_possible == values[1]
    assert new_round.fairways_hit_percentage == round(
        (values[0] / values[1]) * 100, 2)
    assert new_round.greens_in_regulation == values[2]
    assert new_round.greens_in_regulation_percentage == round(
        (values[2] / 18) * 100, 2)
    assert new_round.up_and_downs == values[3]
    if values[2] == 18:
        assert new_round.scrambling_percentage is None
    else:
        assert new_round.scrambling_percentage == round(
            ((values[3] / (18 - values[2])) * 100), 2)
    assert new_round.putts == values[4]
    assert new_round.score == values[5]
    assert new_round.round_number == values[6]


@pytest.mark.parametrize("values", [
    ["Tiger Woods 2000", 71.22, 75.15, 67.08, 28.76, 67.7],
    ["Rory McIlroy 2014", 59.93, 69.44, 58.52, 28.59, 68.8],
    ["Test Model 1", 0.00, 0.00, 0.00, 25.00, 70.0],
    ["Test Model 2", 100.00, 100.00, 100.00, 30.00, 60.0]
])
def test_model(values):
    """
    Tests the ModelProfile class
    """
    model = albatross.ModelProfile(
        values[0], values[1], values[2], values[3], values[4], values[5])
    assert model.name == values[0]
    assert model.fairways_hit_percentage == values[1]
    assert model.greens_in_regulation_percentage == values[2]
    assert model.scrambling_percentage == values[3]
    assert model.putts_per_round == values[4]
    assert model.scoring_average == values[5]


@pytest.mark.parametrize("values", [
    [albatross.Round(12, 14, 15, 2, 28, 67, 1), 14.49, 2.03,
     8.18, 1.47, -0.41, -0.01, -0.76, -0.7],
    [albatross.Round(5, 15, 18, 0, 25, 62, 1), -37.89, -5.3,
     24.85, 4.47, None, None, -3.76, -5.7]
])
def test_compare(values):
    """
    Tests the compare method in PlayerProfile
    """

    model = albatross.ModelProfile("Tiger Woods 2000", 71.22,
                                   75.15, 67.08, 28.76, 67.7)

    player = albatross.PlayerProfile()
    player.update(values[0])

    fairways_hit_percent_comp, fairways_hit_comp, greens_in_regulation_percent_comp, greens_in_regulation_comp, scrambling_percent_comp, scrambling_comp, putts_comp, score_comp = player.compare(
        model)

    assert fairways_hit_percent_comp == values[1]
    assert fairways_hit_comp == values[2]
    assert greens_in_regulation_percent_comp == values[3]
    assert greens_in_regulation_comp == values[4]
    assert scrambling_percent_comp == values[5]
    assert scrambling_comp == values[6]
    assert putts_comp == values[7]
    assert score_comp == values[8]
